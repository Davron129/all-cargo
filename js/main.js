const modal = document.getElementById("modal");
const subscribeBtn = document.getElementById("subscribe");
const span = document.getElementsByClassName("close")[0];
const closeModal = document.getElementById("modal_close");
const nav = document.getElementById("nav");
const logo = document.querySelector(".nav_logo");

console.log("main js worked");
// const fullpage = document.getElementById("fullpage");

// var myFullPage = new fullpage('#fullpage', {
//     //options here
//     autoScrolling:true,
//     scrollHorizontally: true
// });


$('html').bind('mousewheel DOMMouseScroll keydown', function (e) {
    if(!document.getElementById("first-slide").className.includes("active")) {
        nav.classList.add("bg-grey");
        logo.setAttribute("src", "./images/logo-blue.png")
    } else {
        nav.classList.remove("bg-grey");
        logo.setAttribute("src", "./images/logo-white.png")
    }
});

document.getElementById("goToTop").addEventListener("click", () => {
    fullpage_api.moveTo();
    nav.classList.remove("bg-grey");
    logo.setAttribute("src", "./images/logo-white.png")
});

const show_modal = () => {
    console.log("show modal");
    modal.style.display = "flex";
    addActive();
}

const close_modal = () => {
    console.log("close modal");
    modal.style.display = "none";
    addActive();
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        addActive();
    }
}

function showTab(event, index) {
    event.preventDefault();
    let tabs = document.querySelectorAll(".tab");
    tabs.forEach(function (item) {
        item.classList.add("d-none");
        if (item.classList.contains(index)) {
            item.classList.remove("d-none");
        }
    });

    let lis = document.querySelectorAll("#form-navigator > li");
    lis.forEach(function (item) {
        if (event.target.tagName === "LI") {
            item.classList.remove("active");
            event.target.classList.add("active");
        }
    });
}

function addActive() {
    document.querySelector('.subscribe_form').classList.toggle("active");
    document.querySelector('.subscribe_form button').classList.toggle("active");
}
